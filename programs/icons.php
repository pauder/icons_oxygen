<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

bab_functionality::includefile('Icons');


/**
 * Provides the Oxygen icon theme.
 */
class Func_Icons_Oxygen extends Func_Icons
{
	/**
	 * @return string
	 * @static
	 */
	public function getDescription()
	{
		return 'Provides the Oxygen icon theme.';
	}


	/**
	 * Register myself as a functionality.
	 * @static
	 */
	public function register()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
		$functionalities = new bab_functionalities();
		$functionalities->registerClass(__CLASS__, __FILE__);
	}


	/**
	 * Includes all necessary CSS files to the current page.
	 *
	 * @return bool		false in case of error
	 */
	public function includeCss()
	{
		global $babBody;
		if (!isset($babBody))
		{
			return false;
		}
		
		$css = $this->getCss();
		
		if ('vendor' === mb_substr($css, 0, 6)) {
		    $babBody->addStyleSheet($css);
		} else {
		    $babBody->addStyleSheet('addons/icons_oxygen/icons/icons.css');
		}
		
		
		

		$babBody->addStyleSheet($this->getCss());
		return true;
	}

	/**
	 * Returns the css file relative url corresponding to the icon theme.
	 *
	 * @return string
	 */
	public function getCss()
	{
	    $addon = bab_getAddonInfosInstance('icons_oxygen');

	    if (!$addon) {
	        throw new Exception('Failed to retrieve the icons css path because the icon_oxygen addon is not installed');
	    }

		return $addon->getStylePath().'icons/icons.css';
	}



	public function getImagePath($icon, $size)
	{
		/* @var $addon bab_addonInfos */
		$addon = bab_getAddonInfosInstance('icons_oxygen');

		$stylePath = $addon->getStylePath();

		$iconParts = explode('-', $icon);

		$iconCategory = array_shift($iconParts);

		$iconName = implode('-', $iconParts);

		return $stylePath . 'icons/' . $size . 'x' . $size . '/' . $iconCategory . '/' . $iconName . '.png';
	}
}

